package com.devcamp.j52contructor.s10;

public class Person {
    public String name;
    public int age;
    double weight;
    long salary;
    String[] pets;

    public Person() {
        this.name = "Nguyen Duc Nam";
        this.age = 10;
        this.pets = new String[]{"cat", "dog", "pig"};
    }

    public Person(String name) {
        this.name = name;
        this.age = 10;
        //this.pets = new String[]{"cat", "dog", "pig"};
    }

    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        //this.pets = new String[]{"cat", "dog", "pig"};
    }

    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age, weight);
        this.salary = salary;
        this.pets = pets;
    }

    @Override
    public String toString() {
        String result = "Person [name = " + this.name + ", age = " + this.age + ", weight = " + this.weight + ", salary = " + this.salary + ", pets = {";
        if (this.pets != null) {
            for (String pet : this.pets) {
                result += pet + ",";
            }            
        }
        result += "}]";

        return result;
    }
}
