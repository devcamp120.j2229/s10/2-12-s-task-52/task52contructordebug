import java.util.*;
import com.devcamp.j52contructor.s10.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Person person = new Person();
        person.name = "Nguyen Van Nam";
        person.age = 25;
        System.out.println(person.name);
        System.out.println(person.age);
        System.out.println(person);

        Person person2 = new Person();
        person2.name = "Tran Manh Hung";
        person2.age = 27;
        System.out.println(person2.name);
        System.out.println(person2.age);
        System.out.println(person);

        ArrayList<Person> listPerson = new ArrayList<Person>();
        listPerson.add(person2);
        listPerson.add(person);
        listPerson.add(new Person("Tran Manh Thang"));
        listPerson.add(new Person("Nguyen Hien", 33, 60));
        listPerson.add(new Person("Duy Bach", 22, 56, 15000000, new String[] {"cat", "dog"}));

        for (Person per: listPerson) {
            System.out.println(per.toString());
        }
    }
}
